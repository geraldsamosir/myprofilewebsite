# build environment
FROM node:8.10.0 as builder
RUN mkdir /usr/src/app
WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package.json /usr/src/app/package.json
RUN npm install --silent
RUN npm install react-scripts@1.1.1 -g --silent
COPY . /usr/src/app

#sed  configuration
RUN mv /usr/src/app/src/config.template.json  /usr/src/app/src/config.json
RUN sed -i 's|__MEDIUM_API__|https://medium.com|g' /usr/src/app/src/config.json
RUN sed -i 's|__MEDIUM_USER__|@geraldhalomoansamosir|g' /usr/src/app/src/config.json
RUN sed -i 's|__MEDIUM_IMAGE_BANNER_URL__|https://cdn-images-1.medium.com/max/800|g' /usr/src/app/src/config.json
RUN sed -i 's|__MEDIUM_MEDIA_URL__|https://medium.com/media|g' /usr/src/app/src/config.json
RUN sed -i 's|__BEHANCE_API__|https://www.behance.net/v2|g' /usr/src/app/src/config.json
RUN sed -i 's|__BEHANCE_API_KEY__|P9M4EPyfY8orkitWhEoIqFpDzEC87q2R|g' /usr/src/app/src/config.json
RUN sed -i 's|__BEHANCE_USER__|labsopenmi91d4|g' /usr/src/app/src/config.json
RUN sed -i 's|__API_HANDLE_CROSREQUEST__|/apicors|g' /usr/src/app/src/config.json


RUN npm run build

# production environment
FROM nginx:1.15.9-alpine
COPY --from=builder /usr/src/app/nginx/default.conf /etc/nginx/conf.d
COPY --from=builder /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]